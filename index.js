// Siapkan routing express kamu disini...
// jangan lupa listen di port sesuai process.env ya, defaultnya bebas misal 8000


const fs = require("fs")
const express = require("express")
const multer = require("multer")
const form = multer ({dest: "upload"})
const app = express()
const filename = "profile.json"

app.use(express.json())
app.use("/", express.static("."))

app.post("/profile", (req, res) => {
    if (fs.existsSync(filename)) {
        res.status(409).json({ message: "data sudah ada" })
        return
    }
    fs.writeFileSync(filename, JSON.stringify(req.body))
    res.status(201).json({ message: "sukses" })
})

app.get("/profile", (req, res) => {
    if (!fs.existsSync(filename)) {
        res.status(404).json({ message: "data tidak ditemukan" })
        return
    }
    res.status(200).sendFile(__dirname + "/" + filename)
})

app.put("/profile", (req, res) => {
    if (!fs.existsSync(filename)) {
        res.status(404).json({ message: "data tidak ditemukan" })
        return
    }
    let profile = fs.readFileSync(filename, "utf-8")
    let jsonProfile = JSON.parse(profile)
    let jsonPayload = req.body
    jsonProfile = Object.assign(jsonProfile, jsonPayload)
    let updatedProfile = JSON.stringify(jsonProfile)
    fs.writeFileSync(filename, updatedProfile)
    res.status(201).json({ message: "data berhasil diperbarui" })
})

app.delete("/profile", (req, res) => {
    if (!fs.existsSync(filename)) {
        res.status(404).json({ message: "data tidak ditemukan" })
        return
    }
    fs.unlink(filename, () => console.log("data fisik telah dihapus"))
    res.status(204).end("")
})

app.post("/profile-upload", form.single("attachment"),(req, res) => {
    let result = {}

    if (req.file) {
        let originalname = req.file.originalname
        let temporary = req.file.path
        fs.copyFileSync(temporary, `upload/${originalname}`)
        fs.unlinkSync(temporary)
        result.url = `/upload/${originalname}`

    }
    res.json(result)
})

app.listen(9090, () => {
    console.log(`express running on port 9090`)
})
